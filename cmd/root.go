// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/t4spartners/csm-extract/extractor"
	"gitlab.com/t4spartners/csm-go-lib/auth"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "cherwell",
	Short: "Extract data from Cherwell Service Management",
	Run: func(cmd *cobra.Command, args []string) {
		if viper.GetBool("verbose") {
			extractor.Debug()
		}

		l := &extractor.MongoLoader{
			Uri: viper.GetString("mongo.uri"),
			DB:  viper.GetString("mongo.db"),
		}

		e := worpExtractor()
		pipeline := extractor.ETLPipeline{}
		pipeline.WithGenerator(objectGenerator())
		pipeline.WithExtractor(e)
		pipeline.WithTransformer(transformer())
		pipeline.WithLoader(l)

		if viper.GetBool("deamon") {
			var (
				// csm auth initialization
				url    = viper.GetString("cherwell.url")
				apikey = viper.GetString("cherwell.apikey")
				user   = viper.GetString("cherwell.user")
				pass   = viper.GetString("cherwell.pass")
			)

			d := extractor.NewDefaults(url, apikey, user, pass)
			extractor.Listen(&pipeline, l, e.(*extractor.WorpExtractor), viper.GetString("port"), d)
		} else {
			mgoCol := viper.GetStringSlice("objects")
			extractor.Cleanup(l.Uri, l.DB, mgoCol)

			if err := pipeline.Run(); err != nil {
				fmt.Fprintln(os.Stderr, "failed to run extractor:", err)
			}
		}
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")

	RootCmd.PersistentFlags().BoolP("verbose", "v", false, "verbose output")
	viper.BindPFlag("verbose", RootCmd.PersistentFlags().Lookup("verbose"))

	RootCmd.PersistentFlags().BoolP("deamon", "d", false, "run extractor as a deamon")
	viper.BindPFlag("deamon", RootCmd.PersistentFlags().Lookup("deamon"))

	RootCmd.PersistentFlags().StringP("port", "p", "50051", "port for deamon to listen on")
	viper.BindPFlag("port", RootCmd.PersistentFlags().Lookup("port"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	loadDefaultSettings()

	// enviroment configuration
	viper.SetEnvPrefix("CSM")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".cherwell")
	viper.AddConfigPath("$HOME")
	if err := viper.ReadInConfig(); err == nil {
		if viper.GetBool("verbose") {
			fmt.Println("Using config file:", viper.ConfigFileUsed())
		}
	}

	// // etcd configuration
	// viper.AddRemoteProvider("etcd", "http://127.0.0.1:2379", "/config/cherwell.json")
	// viper.SetConfigType("json")
	// if err := viper.ReadRemoteConfig(); err == nil {
	//	if viper.GetBool("verbose") {
	//		fmt.Println("Using remote config")
	//	}
	// }
}

func loadDefaultSettings() {
	// worp defaults
	viper.SetDefault("worp.host", "localhost")
	viper.SetDefault("worp.port", "8080")

	// mongo defaults
	viper.SetDefault("mongo.uri", "localhost")
	viper.SetDefault("mongo.db", "dvr")

	// concurrency defaults
	viper.SetDefault("workers", 500)
	viper.SetDefault("page_size", 500)
}

func objectGenerator() extractor.Generator {
	return &extractor.SimpleGenerator{Objects: viper.GetStringSlice("objects")}
}

func worpExtractor() extractor.Extractor {
	e := &extractor.WorpExtractor{
		Worp:     viper.GetString("worp.host") + ":" + viper.GetString("worp.port"),
		PageSize: viper.GetInt("page_size"),
	}

	var (
		// csm auth initialization
		uri    = viper.GetString("cherwell.url")
		apikey = viper.GetString("cherwell.apikey")
		user   = viper.GetString("cherwell.user")
		pass   = viper.GetString("cherwell.pass")
	)

	return e.WithAuth(auth.NewAuth(uri, apikey, &user, &pass))
}

func transformer() extractor.Transformer {
	return &extractor.EmptyTransformer{}
}

func mongoLoader() extractor.Loader {
	return &extractor.MongoLoader{
		Uri: viper.GetString("mongo.uri"),
		DB:  viper.GetString("mongo.db"),
	}
}
