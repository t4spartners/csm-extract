# Build Container
FROM golang:alpine
RUN apk -U add make git
WORKDIR /go/src/gitlab.com/t4spartners/csm-extract
COPY . .
RUN make deps; make install

# Final Build Image
FROM alpine:latest

COPY --from=0 /go/bin/csm-extract .
ENTRYPOINT ["/csm-extract"]

EXPOSE 50051
