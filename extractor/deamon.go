package extractor

import (
	"net"

	pb "gitlab.com/t4spartners/csm-extract/protobuf"
	"gitlab.com/t4spartners/csm-go-lib/auth"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var version string

// server is used to implement helloworld.GreeterServer.
type server struct {
	pipeline *ETLPipeline
	e        *WorpExtractor
	l        *MongoLoader
	d        *Defaults
}

type Defaults struct {
	Url    string
	Apikey string
	User   string
	Pass   string
}

func NewDefaults(url, apikey, user, pass string) *Defaults {
	return &Defaults{
		Url:    url,
		Apikey: apikey,
		User:   user,
		Pass:   pass,
	}
}

func orDefault(in, def string) string {
	if in != "" {
		return in
	} else {
		return def
	}
}

// Start implements extractor.ExtractService
func (s *server) Start(ctx context.Context, in *pb.CsmExtractRequest) (*pb.CsmExtractResponse, error) {
	info.Println("handling request")

	// ec := *s.e
	if len(in.Objects) != 0 {
		simple, ok := s.pipeline.gen.(*SimpleGenerator)
		if ok {
			simple.Objects = in.Objects
		}
	}

	var (
		url    = orDefault(in.Url, s.d.Url)
		apikey = orDefault(in.Apikey, s.d.Apikey)
		user   = orDefault(in.User, s.d.User)
		pass   = orDefault(in.Passwd, s.d.Pass)
	)

	if in.Ssl {
		s.e.WithAuth(auth.NewAuthSSL(url, apikey, &user, &pass))
	} else {
		s.e.WithAuth(auth.NewAuth(url, apikey, &user, &pass))
	}

	Cleanup(s.l.Uri, s.l.DB, in.Objects)
	err := s.pipeline.Run()
	return &pb.CsmExtractResponse{}, err
}

func (s *server) Info(ctx context.Context, in *pb.CsmInfoRequest) (*pb.CsmInfoResponse, error) {
	return &pb.CsmInfoResponse{Version: version}, nil
}

func Listen(pipeline *ETLPipeline, l *MongoLoader, e *WorpExtractor, port string, d *Defaults) {
	lis, err := net.Listen("tcp", ":"+port)
	if err != nil {
		info.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterCsmExtractServer(s, &server{pipeline: pipeline, l: l, e: e, d: d})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	info.Printf("listening (%v)", port)
	if err := s.Serve(lis); err != nil {
		info.Fatalf("failed to serve: %v", err)
	}
}
