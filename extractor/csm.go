package extractor

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"sync"

	csm "gitlab.com/t4spartners/csm-go-lib"
	"gitlab.com/t4spartners/csm-go-lib/auth"
	"gitlab.com/t4spartners/csm-go-lib/client/business_object"
	"gitlab.com/t4spartners/csm-go-lib/client/searches"
	"gitlab.com/t4spartners/csm-go-lib/models"
)

type CsmExtractor struct {
	c        *csm.Connection
	auth     *auth.Auth
	PageSize int
}

func NewCsmExtractor() *CsmExtractor {
	return &CsmExtractor{PageSize: 500}
}

func (e *CsmExtractor) WithConn(c *csm.Connection) *CsmExtractor {
	e.c = c
	return e
}

func (e *CsmExtractor) WithAuth(a *auth.Auth) *CsmExtractor {
	e.auth = a
	return e
}

func (e *CsmExtractor) Extract(in <-chan Message, errc chan error) (<-chan Message, chan error) {
	out := make(chan Message)

	go func() {
		var wg sync.WaitGroup

		for msg := range in {
			object, ok := msg.Payload.(string)
			if !ok {
				errc <- fmt.Errorf("csm extractor expected string, found %T", msg.Payload)
				continue
			}

			wg.Add(1)
			go func(o string) {
				defer wg.Done()
				i, err := e.count(o)
				if err != nil {
					errc <- err
					return
				}
				debug.Printf("%s count: %d\n", o, i)

				loop := int(math.Ceil(float64(i) / float64(e.PageSize)))
				for i := 0; i < loop; i++ {
					wg.Add(1)
					go func(page int) {
						defer wg.Done()

						data, err := e.getPage(o, page)
						if err != nil {
							errc <- err
							return
						}

						for _, d := range data {
							out <- Message{Payload: d}
						}
					}(i + 1)
				}
			}(object)
		}
		wg.Wait()
		close(out)
	}()
	return out, errc
}

type Object struct {
	Name string
	Data []map[string]interface{}
}

func (e *CsmExtractor) count(object string) (int, error) {
	id, err := e.obId(object)
	if err != nil {
		return 0, err
	}

	sb := csm.NewSearchBuilder().Limit(1).SetObject(id)
	w, _ := e.auth.BearerToken()
	params := searches.NewSearchesGetSearchResultsAdHocV1Params().WithRequest(sb.Search())
	resp, err := e.c.Client.Searches.SearchesGetSearchResultsAdHocV1(params, w)
	if err != nil {
		return 0, err
	}

	return int(resp.Payload.TotalRows), nil
}

func (e *CsmExtractor) getPage(c string, page int) ([]map[string]interface{}, error) {
	id, err := e.obId(c)
	if err != nil {
		return nil, err
	}

	sb := csm.NewSearchBuilder().SetObject(id)
	sb.PageNumber = int32(page)
	sb.PageSize = int32(e.PageSize)
	w, _ := e.auth.BearerToken()
	params := searches.NewSearchesGetSearchResultsAdHocV1Params().WithRequest(sb.Search())
	resp, err := e.c.Client.Searches.SearchesGetSearchResultsAdHocV1(params, w)
	if err != nil {
		return nil, err
	}

	var objects []map[string]interface{}
	for _, object := range resp.Payload.BusinessObjects {
		m := convert(object)
		objects = append(objects, m)
	}
	return objects, nil
}

func convert(rr *models.ReadResponse) (m map[string]interface{}) {
	m = make(map[string]interface{})
	for _, field := range rr.Fields {
		m[field.Name] = field.Value
	}
	return
}

func (e *CsmExtractor) obId(object string) (string, error) {
	params := business_object.NewBusinessObjectGetBusinessObjectSummaryByNameV1Params()
	params.WithBusobname(object)

	w, _ := e.auth.BearerToken()
	resp, err := e.c.BusObSummary(params, w)
	if err != nil {
		return "", err
	}
	if len(resp.Payload) == 0 {
		return "", fmt.Errorf("could not find object")
	}

	return resp.Payload[0].BusObID, nil

}

type CsmLoader struct {
	c    *csm.Connection
	auth *auth.Auth
}

func NewCsmLoader() *CsmLoader {
	return &CsmLoader{}
}

func (e *CsmLoader) WithConn(c *csm.Connection) *CsmLoader {
	e.c = c
	return e
}

func (e *CsmLoader) WithAuth(a *auth.Auth) *CsmLoader {
	e.auth = a
	return e
}

func (e *CsmLoader) Load(in <-chan Message, errc chan error) {
	var wg sync.WaitGroup
	var count int
	// var mut sync.Mutex

	for msg := range in {
		request, ok := msg.Payload.(*models.SaveRequest)
		if !ok {
			errc <- fmt.Errorf("mongo loader expected Object, found %T", msg.Payload)
			continue
		}

		wg.Add(1)
		go func(req *models.SaveRequest) {
			// mut.Lock()
			// defer mut.Unlock()
			defer wg.Done()

			w, err := e.auth.BearerToken()
			if err != nil {
				errc <- fmt.Errorf("could not get bearer token: %s", err)
				return
			}

			request := business_object.NewBusinessObjectSaveBusinessObjectV1Params().WithRequest(req)
			resp, err := e.c.Client.BusinessObject.BusinessObjectSaveBusinessObjectV1(request, w)
			if err != nil {
				errc <- fmt.Errorf("save request failed: %s", err)
				return
			}
			if resp.Payload.ErrorCode != "" {
				errc <- fmt.Errorf("200 error: %v", resp.Payload)
				return
			}
			count++
			debug.Println("save success", count)

		}(request)
	}
	wg.Wait()
	close(errc)
	fmt.Printf("inserted %d records\n", count)
}

type Csv2SaveRequest struct {
	c    *csm.Connection
	auth *auth.Auth

	object   string
	obID     string
	template models.TemplateResponse

	header map[string]int
	ignore map[string]struct{}
}

func NewCsv2SaveRequest() *Csv2SaveRequest {
	return &Csv2SaveRequest{
		ignore: map[string]struct{}{
			"RecID":           struct{}{},
			"CreatedDateTime": struct{}{},
			"CreatedBy":       struct{}{},
			"CreatedByID":     struct{}{},
			"CreatedCulture":  struct{}{},
			"LastModDateTime": struct{}{},
			"LastModBy":       struct{}{},
			"LastModByID":     struct{}{},
		},
	}
}

func (e *Csv2SaveRequest) WithConn(c *csm.Connection) *Csv2SaveRequest {
	e.c = c
	return e
}

func (e *Csv2SaveRequest) WithAuth(a *auth.Auth) *Csv2SaveRequest {
	e.auth = a
	return e
}

func (e *Csv2SaveRequest) WithObject(o string) *Csv2SaveRequest {
	e.object = o
	return e
}

func (e *Csv2SaveRequest) Transform(in <-chan Message, errc chan error) (<-chan Message, chan error) {
	out := make(chan Message)

	go func() {
		for msg := range in {
			row, ok := msg.Payload.([]string)
			if !ok {
				errc <- fmt.Errorf("csv2saverequest expted []string, found %T", msg.Payload)
				return
			}

			if e.header == nil {
				e.initHeader(row)
				continue
			}

			request, err := e.buildSaveRequest(row)
			if err != nil {
				errc <- fmt.Errorf("could not build save request: %s", err)
			}
			out <- Message{Payload: request}
		}
		close(out)
	}()
	return out, errc
}

func (e *Csv2SaveRequest) initHeader(header []string) {
	e.header = make(map[string]int)
	for i, f := range header {
		e.header[f] = i
	}
}

func (e *Csv2SaveRequest) initTemplate() error {
	w, err := e.auth.BearerToken()
	if err != nil {
		return fmt.Errorf("could not get bearer token: %s", err)
	}

	params := business_object.NewBusinessObjectGetBusinessObjectSummaryByNameV1Params()
	params.WithBusobname(e.object)
	resp, err := e.c.BusObSummary(params, w)
	if err != nil {
		return fmt.Errorf("could not retrieve summary: %s", err)
	}
	if len(resp.Payload) == 0 {
		return fmt.Errorf("could not find object: %s", e.object)
	}
	e.obID = resp.Payload[0].BusObID

	templateRequest := &models.TemplateRequest{BusObID: e.obID, IncludeAll: true}
	tparam := business_object.NewBusinessObjectGetBusinessObjectTemplateV1Params()
	tparam.WithRequest(templateRequest)
	tresp, err := e.c.BusObTemplate(tparam, w)
	if err != nil {
		log.Fatal(err)
	}
	e.template = *tresp.Payload
	return nil
}

func (e *Csv2SaveRequest) buildSaveRequest(row []string) (*models.SaveRequest, error) {
	if len(e.template.Fields) == 0 {
		e.initTemplate()
	}

	var t []*models.FieldTemplateItem
	b, err := json.Marshal(e.template.Fields)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(b, &t)
	if err != nil {
		return nil, err
	}

	req := &models.SaveRequest{BusObID: e.obID}
	for _, v := range t {
		if _, ignore := e.ignore[v.Name]; ignore {
			continue
		}
		j, ok := e.header[v.Name]
		if !ok {
			continue
		}
		// debug.Println("setting", req.Fields[i].Name, "to", row[j])
		if x := row[j]; x != "" {
			v.Dirty = true
			v.Value = row[j]
			req.Fields = append(req.Fields, v)
		}
	}
	return req, nil
}
