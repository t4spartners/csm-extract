package extractor

type ETLPipeline struct {
	gen       Generator
	extractor Extractor
	transform Transformer
	target    Loader
}

func (e *ETLPipeline) WithGenerator(g Generator) *ETLPipeline {
	e.gen = g
	return e
}

func (e *ETLPipeline) WithExtractor(ex Extractor) *ETLPipeline {
	e.extractor = ex
	return e
}

func (e *ETLPipeline) WithTransformer(t Transformer) *ETLPipeline {
	e.transform = t
	return e
}

func (e *ETLPipeline) WithLoader(l Loader) *ETLPipeline {
	e.target = l
	return e
}

func (e *ETLPipeline) Run() error {
	errc := make(chan error)
	if e.transform == nil {
		e.transform = EmptyTransformer{}
	}

	go e.target.Load(e.transform.Transform(e.extractor.Extract(e.gen.Generate(errc))))
	for err := range errc {
		// fmt.Println(err)
		return err
	}
	return nil
}

type Extractor interface {
	Extract(<-chan Message, chan error) (<-chan Message, chan error)
}

type Generator interface {
	Generate(chan error) (<-chan Message, chan error)
}

type SimpleGenerator struct {
	Objects []string
}

func (g SimpleGenerator) Generate(errc chan error) (<-chan Message, chan error) {
	out := make(chan Message, len(g.Objects))
	for _, o := range g.Objects {
		out <- Message{Payload: o}
	}
	close(out)
	return out, errc
}

func NewSimpleGenerator(o ...string) *SimpleGenerator {
	return &SimpleGenerator{Objects: o}
}

type Loader interface {
	Load(<-chan Message, chan error)
}

type Transformer interface {
	Transform(<-chan Message, chan error) (<-chan Message, chan error)
}

type EmptyTransformer struct{}

func (t EmptyTransformer) Transform(msgc <-chan Message, errc chan error) (<-chan Message, chan error) {
	return msgc, errc
}
