package extractor

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"sync"
)

type CsvLoader struct {
	header []string
	writer io.Writer
}

func NewCsvLoader(w io.Writer) *CsvLoader {
	return &CsvLoader{writer: w}
}

func (c *CsvLoader) WriteHeader(header []string) error {
	c.header = header
	w := csv.NewWriter(c.writer)
	if err := w.Write(header); err != nil {
		return err
	}
	w.Flush()
	return nil
}

func (c *CsvLoader) writeHeader(o Object) {
	var header []string
	for k, _ := range o.Data[0] {
		header = append(header, k)
	}
	c.WriteHeader(header)
}

// Load expects messages payloads to be []string to load into a csv
func (c CsvLoader) Load(in <-chan Message, errc chan error) {
	w := csv.NewWriter(c.writer)

	for msg := range in {
		row, ok := msg.Payload.([]string)
		if !ok {
			errc <- fmt.Errorf("csv expected slice of strings, found %T", msg.Payload)
			continue
		}

		if err := w.Write(row); err != nil {
			errc <- err
		}
	}
	w.Flush()
	close(errc)
}

func index(s []string, val string) int {
	for i, v := range s {
		if v == val {
			return i
		}
	}
	return -1
}

type CsvExtractor struct {
	Object string
	header []string
}

func NewCsvExtractor(o string) *CsvExtractor {
	return &CsvExtractor{Object: o}
}

func (e CsvExtractor) Extract(in <-chan Message, errc chan error) (<-chan Message, chan error) {
	out := make(chan Message)

	go func() {
		var wg sync.WaitGroup

		for msg := range in {
			filename, ok := msg.Payload.(string)
			if !ok {
				errc <- fmt.Errorf("csv extractor expected string, found %T", msg.Payload)
				continue
			}

			wg.Add(1)
			go func(file string) {
				defer wg.Done()
				f, err := os.Open(file)
				if err != nil {
					errc <- err
					return
				}

				records, err := csv.NewReader(f).ReadAll()
				if err != nil {
					errc <- err
					return
				}
				for _, r := range records {
					out <- Message{Payload: r}
				}
			}(filename)
		}
		wg.Wait()
		close(out)
	}()
	return out, errc
}

func (e *CsvExtractor) convert(record []string) Object {
	m := make(map[string]interface{})
	for i, f := range record {
		m[e.header[i]] = f
	}
	return Object{Name: e.Object, Data: []map[string]interface{}{m}}
}

type Map2Csv struct {
	header map[string]int
}

func (t *Map2Csv) Transform(in <-chan Message, errc chan error) (<-chan Message, chan error) {
	out := make(chan Message)

	go func() {
		for msg := range in {
			dict, ok := msg.Payload.(map[string]interface{})
			if !ok {
				errc <- fmt.Errorf("map2csv exptected map[string]interface{}, found %T", msg.Payload)
				continue
			}

			if t.header == nil {
				h := t.initHeader(dict)
				out <- Message{Payload: h}
			}

			out <- Message{Payload: t.convert(dict)}
		}
		close(out)
	}()
	return out, errc
}

func (t *Map2Csv) convert(dict map[string]interface{}) []string {
	row := make([]string, len(dict))
	for k, v := range dict {
		row[t.header[k]] = v.(string)
	}
	return row
}

func (t *Map2Csv) initHeader(dict map[string]interface{}) []string {
	t.header = make(map[string]int)
	h := make([]string, len(dict))

	i := 0
	for k := range dict {
		t.header[k] = i
		h[i] = k
		i++
	}

	return h
}
