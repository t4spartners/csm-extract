package extractor

import (
	"fmt"
	"strings"
	"sync"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const mgoPrefix = "csm_"

type MongoLoader struct {
	Uri string
	DB  string
}

func (m MongoLoader) Load(in <-chan Message, errc chan error) {
	var wg sync.WaitGroup

	debug.Println("attempting to connect:", m.Uri)
	session, err := mgo.Dial(m.Uri)
	if err != nil {
		errc <- err
		return
	}
	debug.Println("connected to mongo:", m.Uri)
	defer session.Close()

	for msg := range in {
		obj, ok := msg.Payload.(Object)
		if !ok {
			errc <- fmt.Errorf("mongo loader expected Object, found %T", msg.Payload)
			continue
		}

		wg.Add(1)
		go func(o Object) {
			c := session.DB(m.DB).C(strings.ToLower(mgoPrefix + o.Name))
			defer wg.Done()
			for _, v := range o.Data {
				if err := c.Insert(v); err != nil {
					errc <- err
				}
			}
			info.Printf("inserted %d %s\n", len(o.Data), o.Name)
		}(obj)
	}
	wg.Wait()
	close(errc)
}

// Cleanup removes all data from mongodb for each root in paths
func Cleanup(uri, db string, objects []string) error {
	debug.Println("cleaning up mongo:", uri)
	session, err := mgo.Dial(uri)
	if err != nil {
		return err
	}
	defer session.Close()

	for _, o := range objects {
		c := session.DB(db).C(strings.ToLower(mgoPrefix + o))
		_, err := c.RemoveAll(bson.M{})
		if err != nil {
			return err
		}
	}
	return nil
}
