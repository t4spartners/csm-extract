package extractor

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"strconv"
	"sync"

	"gitlab.com/t4spartners/csm-go-lib/auth"
)

type WorpExtractor struct {
	// worp connection string
	Worp     string
	PageSize int

	auth   *auth.Auth
	client *http.Client
}

func (e *WorpExtractor) WithAuth(a *auth.Auth) *WorpExtractor {
	e.client = &http.Client{}
	e.auth = a
	return e
}

func (e *WorpExtractor) Extract(in <-chan Message, errc chan error) (<-chan Message, chan error) {
	out := make(chan Message)

	go func() {
		var wg sync.WaitGroup
		for msg := range in {
			object, ok := msg.Payload.(string)
			if !ok {
				errc <- fmt.Errorf("mongo loader expected Object, found %T", msg.Payload)
				continue
			}

			wg.Add(1)
			go func(o string) {
				defer wg.Done()
				i, err := count(e, o)
				if err != nil {
					errc <- err
					return
				}
				debug.Printf("%s count: %d\n", o, i)

				loop := int(math.Ceil(float64(i) / float64(e.PageSize)))
				for i := 0; i < loop; i++ {
					wg.Add(1)
					go func(page int) {
						defer wg.Done()
						data, err := getPage(e, o, page)
						if err != nil {
							errc <- err
							return
						}
						out <- Message{Payload: data}
					}(i + 1)
				}
			}(object)
		}
		wg.Wait()
		close(out)
	}()
	return out, errc
}

func getPage(e *WorpExtractor, c string, page int) (o Object, err error) {
	path := c + "?pagesize=" + strconv.Itoa(e.PageSize) + "&pagenum=" + strconv.Itoa(page)
	resp, err := get(e, path)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	data := []map[string]interface{}{}
	if err = json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return
	}
	return Object{c, data}, err
}

func get(e *WorpExtractor, s string) (*http.Response, error) {
	debug.Printf("[GET] %s\n", "http://"+e.Worp+"/"+s)
	req, err := http.NewRequest("GET", "http://"+e.Worp+"/"+s, nil)
	if err != nil {
		return nil, err
	}

	t, err := e.auth.BearerTokenString()
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+t)
	return e.client.Do(req)
}

func count(e *WorpExtractor, object string) (int, error) {
	resp, err := get(e, object+"s/count")
	if err != nil {
		debug.Println("count got error:", err)
		return 0, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		err, _ := ioutil.ReadAll(resp.Body)
		return 0, fmt.Errorf("invalid status code (%d): %s", resp.StatusCode, err)
	}

	c := map[string]float64{}
	if err := json.NewDecoder(resp.Body).Decode(&c); err != nil {
		return 0, err
	}
	debug.Println(object, "count", c["count"])

	return int(c["count"]), nil
}
