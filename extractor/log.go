package extractor

import (
	"io/ioutil"
	"log"
	"os"

	"github.com/fatih/color"
)

var (
	// loggers
	debug = log.New(ioutil.Discard, "", log.Lshortfile)
	info  = log.New(os.Stdout, "", log.LstdFlags)
)

func Debug() {
	debug = log.New(os.Stdout, "", log.Lshortfile)
	debug.SetPrefix(color.WhiteString("DEBUG "))
	info.SetPrefix(color.BlueString("INFO "))
}
